import requests, re, random

def mergelists(lists):
    dictlist={}
    for now in lists:
        for i in range(len(now)):
            if now[i] in dictlist:
                dictlist[now[i]]=min([dictlist[now[i]], i+1])/2
            else:
                dictlist[now[i]]=i+1
    endlist=[]
    for i in dictlist:
        endlist+=[[dictlist[i], i]]
    endlist=sorted(endlist)
    finallist=[]
    for i in endlist:
        finallist+=[i[1]]
    return finallist


def ScrapeWeb(times, save=True, merge=True, versions=['en']):
    allwords={}
    for i in range(times):
        version=random.choice(versions)
        url='https://'+version+'.wikipedia.org/wiki/Special:Random/'
        print(i, len(allwords))
        r=requests.get(url)
        print(r.url)
        r=' '.join(r.text.lower().splitlines())
        if '<body' in r:
            r=re.findall(r'<body(.*?)body>', r)[0]
        removable=[
['<script', 'script>'],
['<style', 'style>'],
['<', '>'],
]
        for a in removable:
            now=re.findall(r''+a[0]+'(.*?)'+a[1], r)
            for z in now:
                r=r.replace(a[0]+z+a[1], ' ')
        for a in set(r):
            if a not in 'qwertyuiopasdfghjklzxcvbnm ':
                r=r.replace(a, ' ')
        for a in r.split():
            if a.isalpha() and len(a)>2 and 'wg' not in a:
                if a in allwords:
                    allwords[a]+=1
                else:
                    allwords[a]=1
    result=[]
    for i in allwords:
        result+=[[allwords[i], i]]
    result=sorted(result, reverse=True)
    allwords=[]
    for i in result:
        allwords+=[i[1]]
    if merge:
        file=open('allwords.txt', 'r')
        allwords=[eval(file.read()), allwords]
        file.close()
        allwords=mergelists(allwords)
    if save:
        file=open('allwords.txt', 'w')
        file.write(repr(allwords))
        file.close()
        file=open('allwords.txt', 'r')
        file.read()
        file.close()
        return len(allwords)
    else:
        return allwords
    
