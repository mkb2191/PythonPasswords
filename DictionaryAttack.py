def getwords():
    file=open('words.txt', 'r')
    words=file.read().splitlines()
    file.close()
    file=open('allwords.txt', 'r')
    allwords=eval(file.read())
    file.close()
    for i in words:
        if i in allwords:
            allwords.remove(i)
    return words+allwords

def getreplacements():
    file=open('replacements.txt', 'r')
    replacements=eval(file.read())
    file.close()
    return replacements

def replace(word):
    allwords=['']
    replacements=getreplacements()
    for i in word:
        now=[i]
        if i in replacements:
            for a in replacements[i]:
                now+=[a]
        alllists=[allwords.copy()]
        for a in range(len(now)-1):
            alllists+=[allwords.copy()]
        for a in range(len(now)):
            for z in range(len(allwords)):
                alllists[a][z]+=now[a]
        allwords=[]
        for i in alllists:
            allwords+=i
    return allwords

def change(word):
    allwords=[word]
    if word[0].isalpha():
        allwords+=[word[0].upper()+word[1:]]
    for i in [1, 2, 3, 12, 7, 5, 4, 6, 9, 8, 123]:
        allwords+=[word+str(i)]
        if word[0].isalpha():
            now=word[0].upper()+word[1:]
            allwords+=[now+str(i)]
    return allwords

def dictionaryattack():
    number=''
    for i in range(10):
        number+=str(i+1)[-1]
        print(number)
    words=getwords()
    for word in words:
        new=[]
        for i in replace(word):
            new+=change(i)
        for i in new:
            print(i)
