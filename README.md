# PythonPasswords
This Python3 file is designed to do dictionary attacks using any word list that is sorted in frequency. Ideally you would have a list of most common base words and a larger list of the most common words in English.
I am using the password infomation available on http://wpengine.com/unmasked/

This program prints the passwords, so you can pipe them in to any program of your choosing. This means that it can be very versitile.
